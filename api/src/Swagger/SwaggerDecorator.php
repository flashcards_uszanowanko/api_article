<?php

declare(strict_types=1);

namespace App\Swagger;


use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class SwaggerDecorator implements NormalizerInterface
{
    private $decorated;

    public function __construct(NormalizerInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $docs = $this->decorated->normalize($object, $format, $context);
        $docs['paths']['/login'] = [
            'post' => [
                'tags' => ['login'],
                'summary' => 'Login to app',
                'parameters' => [
                    [
                        'name' => 'login',
                        'in' => 'body',
                        'schema' => [
                            'type' => 'object',
                            'required' => ['email', 'password'],
                            'properties' => [
                                'email' => ['type' => 'string'],
                                'password' => ['type' => 'string']
                            ],
                        ]
                    ]
                ],
                'responses' => [
                    200 => [
                        'description' => 'User logged in',
                        'schema' => [
                            'type' => 'object',
                            'properties' => ['token' => ['type' => 'string']],
                        ]
                    ],
                    401 => [
                        'description' => 'Bad credentials',
                        'schema' => [
                            'type' => 'object',
                            'properties' => [
                                'code' => ['type' => 'integer'],
                                'message' => ['type' => 'string']
                            ],
                        ]
                    ]
                ]
            ]
        ];

        return $docs;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $this->decorated->supportsNormalization($data, $format);
    }
}
